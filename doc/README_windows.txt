Bunkercoin 1.0.0 BETA

Copyright (c) 2009-2014 Bitcoin Core Developers

Distributed under the MIT/X11 software license, see the accompanying
file COPYING or http://www.opensource.org/licenses/mit-license.php.
This product includes software developed by the OpenSSL Project for use in
the OpenSSL Toolkit (http://www.openssl.org/).  This product includes
cryptographic software written by Eric Young (eay@cryptsoft.com).


Intro
-----
Bunkercoin is a cryptocurrency like Bitcoin, although it does not 
use SHA256 as its proof of work (POW). Taking development cues from Dogecoin and Bitcoin, 
Bunkercoin currently employs a simplified variant of scrypt.


Setup
-----
Unpack the files into a directory and run bunkercoin-qt.exe.

Bunkercoin Core is the original Bunkercoin client and it builds the backbone of the network.
However, it downloads and stores the entire history of Bunkercoin transactions;
depending on the speed of your computer and network connection, the synchronization
process can take anywhere from a few hours to a day or more.

See the Bunkercoin wiki at:
4r
for more help and information.
